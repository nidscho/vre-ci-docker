FROM mongo:5.0.8-focal

RUN apt update -qq && apt install --yes -qq \
  python3 \
  python3-pip

RUN pip install --upgrade pip

RUN pip install flake8 pylint pylint-exit anybadge fireworks prettytable ipywidgets ipyfilechooser ipython fabric2 pyflakes testfixtures bandit[toml] pytest
